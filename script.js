const API_URL = 'https://api.github.com'
const USER_LIMIT = 20;
let usersSearch = document.getElementById('search_users')
let repoSearch  = document.getElementById('repositories_search') 
if(usersSearch){
        usersSearch.addEventListener('click',function(){
        let q = document.getElementById('user_query').value
        let url = new URL(API_URL+'/search/users')
        url.search = new URLSearchParams({q}).toString();
        
        let paginator = getPageinationData('/search/users',{q,page:1,callback:'handeUsersResponse'})

        history.pushState({url:url.href,callback:'handeUsersResponse',selector:'users-container'}, "Test Task", url.search)
        document.getElementById('users-container').innerHTML = ""
        sendApiRequest(url,paginator,handeUsersResponse)
    })
}
if(repoSearch){
    repoSearch.addEventListener('click',function(){
        let endPoint = '/search/repositories'
        let q = document.getElementById('user_query').value
        let url = new URL(API_URL+endPoint)
        url.search = new URLSearchParams({q}).toString();
        let paginator = getPageinationData(endPoint,{q,page:1,callback:'handeRepoResponse'})

        history.pushState({url:url.href,callback:'handeRepoResponse',selector:'users-container'}, "Test Task", url.search)
        document.getElementById('users-container').innerHTML = ""
        sendApiRequest(url,paginator,handeRepoResponse)
    })
}


function pageinationButtonsClickEvent(event){
    let url      = event.target.getAttribute("data-url");
    let endpoint = event.target.getAttribute('data-endpoint')
    let callback = event.target.getAttribute('data-callback')
    let urlObject =  new URL(url)
    let  params = new URLSearchParams(urlObject.search);
    let obj = {q:params.get('q'),page:+params.get('page'),callback}
    let paginator = getPageinationData(endpoint,obj)
    
    history.pushState({url:urlObject.href,callback,selector:'users-container',endpoint,}, "Test Task", urlObject.search)
    document.getElementById('users-container').innerHTML = ""
    let callbackFunc = window[callback]
    sendApiRequest(url,paginator,callbackFunc)
}

function sendApiRequest(url,paginator,callback){
    fetch(url).then( response =>{
        response.json().then(function(data) {
            callback(data,paginator)
        });
    })
}

function getPageinationData(endpoint,data){
    
    let page = data.page
    let prevPage = page-1;
    let nextPage = page+1
    let paginator = {}
    paginator.callback = data.callback
    paginator.endpoint = endpoint
    let next = new URL(API_URL+endpoint)
    next.search = new URLSearchParams({q:data.q,page:nextPage})
    paginator.next = next
    if(data.page>1){
        let page = data.page
        let prev = new URL(API_URL+endpoint)
        prev.search = new URLSearchParams({q:data.q,page:prevPage})
        paginator.prev = prev
    }
    

    return paginator;
}

function handeUsersResponse(result,paginator){
    if(!result.items){
        showMessage('danger',result.message ? result.message : 'Something Went Wrong.')
    }
    if(result.items.length==0){
        showMessage('warning','No results')
        return
    }
    let mainContainer = document.getElementById('users-container')
    if(result.items){
        let showNext = result.items.length == 30 ? true : false
        generatePageination(paginator,showNext)
        result.items.map(item =>{
            let container = document.createElement('div')
            container.classList.add('col-md-4')

            let card = document.createElement('div')
            card.classList.add('card','p-2')

            let cardBlock = document.createElement('div')
            cardBlock.classList.add('card-body')

            let avatar = document.createElement('img')
            avatar.setAttribute('src',item.avatar_url)
            avatar.classList.add('img-fluid')

            let textContainer  = document.createElement('h5')
            textContainer.classList.add('card-title','text-center')

            let userLink = document.createElement('a')
            userLink.innerText = item.login

            container.appendChild(avatar)
            container.appendChild(cardBlock)
            cardBlock.appendChild(textContainer)
            textContainer.appendChild(userLink)

            mainContainer.appendChild(container)
        })
    }

}

function handeRepoResponse(result ,paginator){
    if(!result.items){
        showMessage('danger',result.message ? result.message : 'Something Went Wrong.')
    }
    if(result.items.length==0){
        showMessage('warning','No results')
        return
    }
    if(result.items){
        let showNext = result.items.length == 30 ? true : false
        generatePageination(paginator,showNext)
        let mainContainer = document.getElementById('users-container')
        result.items.map(item =>{
            let container = document.createElement('div')
            container.classList.add('col-md-4')
    
            let card = document.createElement('div')
            card.classList.add('card','p-2')
    
            let cardBlock = document.createElement('div')
            cardBlock.classList.add('card-body')
    
           
    
            let textContainer  = document.createElement('h5')
            textContainer.classList.add('card-title','text-center')
            // textContainer.innerText = item.name
    
            let link = document.createElement('a')
            link.innerText = item.name
            link.setAttribute('target','_blank')
            link.setAttribute('href',item.clone_url)
     
            container.appendChild(cardBlock)
            cardBlock.appendChild(textContainer)
            textContainer.appendChild(link)
    
            mainContainer.appendChild(container)
        })
    }
    
    console.log(result)
}
function generatePageination(paginator,showNext){
    let container = document.getElementById('pageing')
    container.innerHTML = ''
    if(paginator.prev){
        let prevButton = document.createElement('button')
        prevButton.classList.add('btn','btn-secondary','pagination-button')
        prevButton.innerText = '<< Prev'
        prevButton.setAttribute('data-url',paginator.prev)
        prevButton.setAttribute('data-callback',paginator.callback)
        prevButton.setAttribute('data-endpoint',paginator.endpoint)
        container.appendChild(prevButton)
    }
    let nextButton = document.createElement('button')
    if(showNext){
        nextButton.classList.add('btn','btn-primary','pagination-button')
        nextButton.innerText = 'Next >>'
        nextButton.setAttribute('data-url',paginator.next)
        nextButton.setAttribute('data-callback',paginator.callback)
        nextButton.setAttribute('data-endpoint',paginator.endpoint)
        container.appendChild(nextButton)
    }
    let pageinationButtons = document.querySelectorAll('.pagination-button')
    pageinationButtons.forEach(function(elem) {
        elem.addEventListener("click",pageinationButtonsClickEvent);
    });
  


}

function generateRequestUrl(endpoint,params){
    let url = API_URL+endpoint+'?q='+search
}

function showMessage(type,message){
    let container = document.getElementById('msg-container')
    let messageMark = document.createElement('div')
    let className = 'alert-'+type
    messageMark.classList.add('alert',className)
    messageMark.innerText = message
    container.appendChild(messageMark)
    setTimeout(function(){
        messageMark.remove()
    },5000)
}

window.addEventListener('popstate', (event) => {
    
    if(event.state){
        //set state
        let container = document.getElementById(event.state.selector)
        container.innerHTML = '';
         
        //for passing callback as string
        let urlObject =  new URL(event.state.url)

        let  params = new URLSearchParams(urlObject.search);
        document.getElementById('user_query').value = params.get('q')
        let obj = {q:params.get('q'),page:+params.get('page'),callback:event.state.callback}
        let paginator = getPageinationData(event.state.endpoint,obj)
        
        let callback = window[event.state.callback]
        
        sendApiRequest(event.state.url,paginator,callback)
    }else{
        let container = document.getElementById(event.state.selector)
        container.innerHTML = '';
        document.getElementById('user_query').value = ''
    }
});